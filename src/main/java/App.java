package main.java;

import main.java.attributes.Attributes;
import main.java.character.Character;
import main.java.character.Mage;
import main.java.character.Ranger;
import main.java.character.Rogue;
import main.java.character.Warrior;
import main.java.exception.InvalidArmorException;
import main.java.exception.InvalidWeaponException;
import main.java.item.Armor;
import main.java.item.Item;
import main.java.item.Weapon;

//Some random manual testing in here
public class App {
    public static void main(String[] args) throws InvalidWeaponException, InvalidArmorException {
        Ranger ranger = new Ranger("ranger");
        //Mage mage = new Mage("mage");

        System.out.println(ranger.displayStats());
        ranger.levelUp();
        System.out.println(ranger.displayStats());

        Attributes sampleAttr = new Attributes(1,1,1);
        Attributes sampleAttr2 = new Attributes(2,2,2);
        Armor sampleArmor = new Armor("Leather armor",1, Item.Slot.HEAD, Armor.ArmorType.LEATHER,sampleAttr);
        //Armor sampleArmor2 = new Armor("basic cloth",2, Item.Slot.BODY, Armor.ArmorType.CLOTH,sampleAttr2);
        Armor sampleArmor3 = new Armor("advanved Leather armor",1, Item.Slot.BODY, Armor.ArmorType.LEATHER,sampleAttr2);
        System.out.println("\nranger equip stats:");
        ranger.equip(sampleArmor);
        System.out.println(ranger.displayStats());


        System.out.println("\nranger equip2 stats:");
        ranger.equip(sampleArmor3);
        System.out.println(ranger.displayStats());


        //Weapon sampleAxe = new Weapon("Wooden Axe", 2, Weapon.Slot.WEAPON,
        //        Weapon.WeaponType.AXE,12,1.2);
        Weapon sampleBow = new Weapon("Wooden Bow", 1, Weapon.Slot.WEAPON,
                Weapon.WeaponType.BOW,12,1.2);
        ranger.equip(sampleBow);
        System.out.println(ranger.displayStats());
    }
}
