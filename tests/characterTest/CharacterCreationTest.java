package characterTest;

import main.java.character.Mage;
import main.java.character.Ranger;
import main.java.character.Rogue;
import main.java.character.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CharacterCreationTest {

    //Test that mage is created right
    @Test
    void testMageCorrectness() {
        //Arrange
        Mage testMage = new Mage("TestMage");
        int mStr = 1, mDex = 1, mInt = 8, mLvl = 1;
        int level = testMage.getLevel();
        int strength = testMage.getTotalAttribute().getStrength();
        int dexterity = testMage.getTotalAttribute().getDexterity();
        int intelligence = testMage.getTotalAttribute().getIntelligence();
        //Assert
        assertEquals(mStr,strength, "Wrong stat");
        assertEquals(mDex,dexterity, "Wrong stat");
        assertEquals(mInt,intelligence, "Wrong stat");
        assertEquals(mLvl, level, "Wrong level");

    }

    //Test that ranger is created right
    @Test
    void testRangerCorrectness(){
        //Arrange
        Ranger testRanger = new Ranger("TestRanger");
        int rStr = 1, rDex = 7, rInt = 1, rLvl = 1;
        int level = testRanger.getLevel();
        int strength = testRanger.getTotalAttribute().getStrength();
        int dexterity = testRanger.getTotalAttribute().getDexterity();
        int intelligence = testRanger.getTotalAttribute().getIntelligence();
        //Assert
        assertEquals(rStr,strength, "Wrong stat");
        assertEquals(rDex,dexterity, "Wrong stat");
        assertEquals(rInt,intelligence, "Wrong stat");
        assertEquals(rLvl, level, "Wrong level");
    }

    //Test that rogue is created right
    @Test
    void testRogueCorrectness(){
        //Arrange
        int rStr = 2, rDex = 6, rInt = 1, rLvl = 1;
        Rogue testRogue = new Rogue("TestRogue");
        int level = testRogue.getLevel();
        int strength = testRogue.getTotalAttribute().getStrength();
        int dexterity = testRogue.getTotalAttribute().getDexterity();
        int intelligence = testRogue.getTotalAttribute().getIntelligence();
        //Assert
        assertEquals(rStr,strength, "Wrong stat");
        assertEquals(rDex,dexterity, "Wrong stat");
        assertEquals(rInt,intelligence, "Wrong stat");
        assertEquals(rLvl, level, "Wrong level");
    }

    //Test that warrior is created right
    @Test
    void TestWarriorCorrectness(){
        //Arrange
        Warrior testWarrior = new Warrior("TestWarrior");
        int wStr = 5, wDex = 2, wInt = 1, wLvl = 1;
        int level = testWarrior.getLevel();
        int strength = testWarrior.getTotalAttribute().getStrength();
        int dexterity = testWarrior.getTotalAttribute().getDexterity();
        int intelligence = testWarrior.getTotalAttribute().getIntelligence();
        //Assert
        assertEquals(wStr,strength, "Wrong stat");
        assertEquals(wDex,dexterity, "Wrong stat");
        assertEquals(wInt,intelligence, "Wrong stat");
        assertEquals(wLvl, level, "Wrong level");
    }
}
