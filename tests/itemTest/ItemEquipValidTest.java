package itemTest;

import main.java.attributes.Attributes;
import main.java.character.Warrior;
import main.java.exception.InvalidArmorException;
import main.java.exception.InvalidWeaponException;
import main.java.item.Armor;
import main.java.item.Item;
import main.java.item.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemEquipValidTest {

    //Test for natural DPS without weapon
    @Test
    void testDPSWithoutWeapon() {
        //Arrange
        Warrior testWarrior = new Warrior("TestWarrior");
        double expectedDPS = 1*(1+(5/100.00));
        //Act
        double result = testWarrior.attack();
        //Assert
        assertEquals(result, expectedDPS, "Wrong DPS");
    }

    //Test for equipping legal weapon to character
    @Test
    void testEquipLegalWeapon() throws InvalidWeaponException {
        //Arrange
        Warrior testWarrior = new Warrior("TestWarrior");
        Weapon testWeapon = new Weapon("Iron Axe", 1, Weapon.Slot.WEAPON,
                Weapon.WeaponType.AXE,12,1.2);
        boolean expectedBoolean = true;
        //Act
        boolean equipResult = testWarrior.equip(testWeapon);
        //Assert
        assertEquals(equipResult, expectedBoolean, "Should be true");
    }

    //Test for DPS with weapon equipped to character
    @Test
    void testLegalWeaponDPS() throws InvalidWeaponException {
        //Arrenge
        Warrior testWarrior = new Warrior("TestWarrior");
        Weapon testWeapon = new Weapon("Iron Axe", 1, Weapon.Slot.WEAPON,
                Weapon.WeaponType.AXE,12,1.2);
        double expectedDps = 12*1.2*(1+(5/100.00));
        //Act
        testWarrior.equip(testWeapon);
        double weaponDPS = testWarrior.attack();
        //Assert
        assertEquals(weaponDPS, expectedDps, "Wrong DPS");

    }

    //Test for equipping legal armor to character
    @Test
    void testEquipLegalArmor() throws InvalidArmorException {
        //Arrange
        Warrior testWarrior = new Warrior("TestWarrior");
        Attributes testAttr = new Attributes(2,1,2);
        Armor testArmor = new Armor("Common Plate",1, Item.Slot.BODY, Armor.ArmorType.PLATE, testAttr);
        boolean expectedBoolean = true;
        //Act
        boolean equipResult = testWarrior.equip(testArmor);
        //Assert
        assertEquals(equipResult, expectedBoolean, "Should be true");
    }

    //Test for total DPS with armor and weapon equipped to character
    @Test
    void testDPSWithArmorAndWeapon() throws InvalidArmorException, InvalidWeaponException {
        //Arrange
        Warrior testWarrior = new Warrior("TestWarrior");
        Weapon testWeapon = new Weapon("Iron Axe", 1, Weapon.Slot.WEAPON,
                Weapon.WeaponType.AXE,12,1.2);
        Attributes testAttr = new Attributes(2,1,2);
        Armor testArmor = new Armor("Common Plate",1, Item.Slot.BODY, Armor.ArmorType.PLATE, testAttr);
        double expectedTotalDPS = 12*1.2*(1+(7/100.00));
        //Act
        testWarrior.equip(testArmor);
        testWarrior.equip(testWeapon);
        double providedDPS = testWarrior.attack();
        //Assert
        assertEquals(providedDPS, expectedTotalDPS, "Wrong DPS");

    }
}
