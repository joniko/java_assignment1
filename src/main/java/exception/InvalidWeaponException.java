package main.java.exception;

//Own exception which is used to trigger if illegal weapon is tried to equip.
public class InvalidWeaponException extends Exception{
    public InvalidWeaponException(){ super("You tried to equip illegal Weapon"); }
}
