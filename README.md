# Java Assignment 1: RPG Game

## Background
This assignment is part of Java Fullstack course which is offered by Noroff.  The task was to create classes for RPG game and some jUnit5 testing to test those classes and methods included in the classes.
Requirements were that there should be four types of characters and two types of items. Items are separated to armors and weapons. Each character can equip up to three armors to the matching slots (Head, Body and Legs) and one weapon. Every character and armor have a set of attributes/stats (Strength, Dexterity and Intelligence) included in them. 
Requirements for the methods were to get damage output or the stats changes through the equipping or leveling up.

All the character types/classes are:
* Mage
* Ranger
* Rogue
* Warrior

Weapon types are:
* Axe
* Bow
* Dagger
* Hammer
* Staff
* Sword
* Wand

Armor types are:
* Cloth
* Leather
* Mail
* Plate

## About My Solution
There is made abstract class called Character which works mostly as a helper class to the child classes. There is coded few helping methods to avoid some duplicate code. This class uses interface, where all the needed methods are declared. All the character types are classes that extends the Character class, and they hold most of the functional methods.
Item class is made abstract, and it holds all the available item slot. Both Armor and Weapon class extends the Item class but includes their own characteristics such as types.
Attributes are made as their own class which is use by the armors and characters.
There are also made few own exceptions for illegal equipping and the tests folder includes some jUnit5 test which were required.

## Usage
Clone the repository via SSH or HTTPS to your own computer and use it. There is no functional game in this, but there is class called App where the main method is. So, there you can play with object creation. Also, the jUnit tests are runnable.

## Contributors
* This code and solution were made by me Joni (Gitlab username: joniko).
* Assignment was created by Noroff. [Noroff](https://www.noroff.no/en/)
