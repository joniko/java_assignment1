package main.java.item;

import main.java.attributes.Attributes;

//Armor class to create armor objects. Includes also armor types.
public class Armor extends Item {

    public enum ArmorType{
        CLOTH,
        LEATHER,
        MAIL,
        PLATE,
        WEAPON
    }

    private String name;
    private int level;
    private Slot slot;
    private ArmorType type;
    private Attributes armorAttributes;

    //constructor
    public Armor(String name, int level, Slot slot, ArmorType type, Attributes armorAttributes) {
        super(name, level, slot);
        this.type = type;
        this.armorAttributes = armorAttributes;
    }


    //getter and setters
    public ArmorType getType() {
        return type;
    }

    public void setType(ArmorType type) {
        this.type = type;
    }

    public Attributes getArmorAttributes() {
        return armorAttributes;
    }

    public void setArmorAttributes(Attributes armorAttributes) {
        this.armorAttributes = armorAttributes;
    }
}
