package main.java.item;

//Armor class to create weapon objects. Includes also weapon types.
public class Weapon extends Item{

    public enum WeaponType{
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }

    private String name;
    private int level;
    private Slot slot;
    private WeaponType type;
    private int damage;
    private double attackSpeed;

    //constructor
    public Weapon(String name, int level, Slot slot, WeaponType type, int damage, double attackSpeed) {
        super(name, level, slot);
        this.type = type;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    //Method that gives weapon DPS. Formula: Damage * Attack Speed.
    //Is used to help count total DPS in characters.
    public double getWeaponDPS(){
        return damage * attackSpeed;
    }

    //getter and setters
    public WeaponType getType() {
        return type;
    }

    public void setType(WeaponType type) {
        this.type = type;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }
}
