package itemTest;

import main.java.attributes.Attributes;
import main.java.character.Warrior;
import main.java.exception.InvalidArmorException;
import main.java.exception.InvalidWeaponException;
import main.java.item.Armor;
import main.java.item.Item;
import main.java.item.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ItemExceptionTest {

    //Try to equip too high level weapon to character
    @Test
    void testWrongLevelInvalidWeaponException() {
        //Arrange
        Warrior testWarrior = new Warrior("TestWarrior");
        Weapon tooHighWeapon = new Weapon("Iron Axe", 2, Weapon.Slot.WEAPON,
                Weapon.WeaponType.AXE,12,1.2);
        String msg = "You tried to equip illegal Weapon";
        //Act
        Throwable exception = assertThrows(InvalidWeaponException.class, () -> testWarrior.equip(tooHighWeapon));
        String exmsg = exception.getMessage();
        //Assert
        assertEquals(msg, exmsg, "Wrong exception");
    }

    //Try to equip wrong type of weapon to character
    @Test
    void testWrongTypeInvalidWeaponException(){
        //Arrange
        Warrior testWarrior = new Warrior("TestWarrior");
        Weapon wrongTypeWeapon = new Weapon("Wooden Bow", 1, Weapon.Slot.WEAPON,
                Weapon.WeaponType.BOW,8,1.4);
        String msg = "You tried to equip illegal Weapon";
        //Act
        Throwable exception = assertThrows(InvalidWeaponException.class, () -> testWarrior.equip(wrongTypeWeapon));
        String exmsg = exception.getMessage();
        //Assert
        assertEquals(msg, exmsg, "Wrong exception");
    }

    //Try to equip too high level armor to character
    @Test
    void testWrongLevelInvalidArmorException(){
        //Arrange
        Warrior testWarrior = new Warrior("TestWarrior");
        Attributes armorAttr = new Attributes(2,2,2);
        Armor tooHighArmor = new Armor("Common Plate",2, Item.Slot.BODY, Armor.ArmorType.PLATE, armorAttr);
        String msg = "You tried to equip illegal Armor";
        //Act
        Throwable exception = assertThrows(InvalidArmorException.class, () -> testWarrior.equip(tooHighArmor));
        String exmsg = exception.getMessage();
        //Assert
        assertEquals(msg, exmsg, "Wrong exception");
    }

    //Try to equip wrong type of armor to character
    @Test
    void testWrongTypeInvalidArmorException(){
        //Arrange
        Warrior testWarrior = new Warrior("TestWarrior");
        Attributes armorAttr = new Attributes(2,2,2);
        Armor wrongTypeArmor = new Armor("Common Cloth",1, Item.Slot.HEAD, Armor.ArmorType.CLOTH, armorAttr);
        String msg = "You tried to equip illegal Armor";
        //Act
        Throwable exception = assertThrows(InvalidArmorException.class, () -> testWarrior.equip(wrongTypeArmor));
        String exmsg = exception.getMessage();
        //Assert
        assertEquals(msg, exmsg, "Wrong exception");
    }
}
