package characterTest;

import main.java.character.Mage;
import main.java.character.Ranger;
import main.java.character.Rogue;
import main.java.character.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CharacterLevelUpTest {

    //Test that mage levels up right
    @Test
    void testMageLevelUp() {
        //Arrange
        Mage testMage = new Mage("TestMage");
        int mStr = 2, mDex = 2, mInt = 13, mLvl = 2;
        //Act
        testMage.levelUp();
        int level = testMage.getLevel();
        int strength = testMage.getTotalAttribute().getStrength();
        int dexterity = testMage.getTotalAttribute().getDexterity();
        int intelligence = testMage.getTotalAttribute().getIntelligence();
        //Assert
        assertEquals(mStr, strength, "Wrong stat");
        assertEquals(mDex, dexterity, "Wrong stat");
        assertEquals(mInt, intelligence, "Wrong stat");
        assertEquals(mLvl, level, "Wrong level");
    }

    //Test that ranger levels up right
    @Test
    void testRangerLevelUp(){
        //Arrange
        Ranger testRanger = new Ranger("TestRanger");
        int rStr = 2, rDex = 12, rInt = 2, rLvl = 2;
        //Act
        testRanger.levelUp();
        int level = testRanger.getLevel();
        int strength = testRanger.getTotalAttribute().getStrength();
        int dexterity = testRanger.getTotalAttribute().getDexterity();
        int intelligence = testRanger.getTotalAttribute().getIntelligence();
        //Assert
        assertEquals(rStr,strength, "Wrong stat");
        assertEquals(rDex,dexterity, "Wrong stat");
        assertEquals(rInt,intelligence, "Wrong stat");
        assertEquals(rLvl, level, "Wrong level");

    }

    //Test that rogue levels up right
    @Test
    void testRogueLevelUp(){
        //Arrange
        Rogue testRogue = new Rogue("TestRogue");
        int rStr = 3, rDex = 10, rInt = 2, rLvl = 2;
        //Act
        testRogue.levelUp();
        int level = testRogue.getLevel();
        int strength = testRogue.getTotalAttribute().getStrength();
        int dexterity = testRogue.getTotalAttribute().getDexterity();
        int intelligence = testRogue.getTotalAttribute().getIntelligence();
        //Assert
        assertEquals(rStr,strength, "Wrong stat");
        assertEquals(rDex,dexterity, "Wrong stat");
        assertEquals(rInt,intelligence, "Wrong stat");
        assertEquals(rLvl, level, "Wrong level");

    }

    //Test that warrior levels up right
    @Test
    void testWarriorLevelUp(){
        //Arrange
        Warrior testWarrior = new Warrior("TestWarrior");
        int wStr = 8, wDex = 4, wInt = 2, wLvl = 2;
        //Act
        testWarrior.levelUp();
        int level = testWarrior.getLevel();
        int strength = testWarrior.getTotalAttribute().getStrength();
        int dexterity = testWarrior.getTotalAttribute().getDexterity();
        int intelligence = testWarrior.getTotalAttribute().getIntelligence();
        //Assert
        assertEquals(wStr,strength, "Wrong stat");
        assertEquals(wDex,dexterity, "Wrong stat");
        assertEquals(wInt,intelligence, "Wrong stat");
        assertEquals(wLvl, level, "Wrong level");

    }
}
