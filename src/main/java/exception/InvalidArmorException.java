package main.java.exception;

//Own exception which is used to trigger if illegal armor is tried to equip.
public class InvalidArmorException extends Exception{
    public InvalidArmorException(){
        super("You tried to equip illegal Armor");
    }
}
