package main.java.attributes;

import java.util.HashMap;

//Class where you can make object of attributes

public class Attributes {
    private int strength;
    private int dexterity;
    private int intelligence;

    //constructor
    public Attributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    //method which adds to all attributes
    public void addAttributes(int strength, int dexterity, int intelligence){
        this.strength += strength;
        this.dexterity += dexterity;
        this.intelligence += intelligence;
    }

    //method which decreases all attributes
    public void removeAttributes(int strength, int dexterity, int intelligence){
        this.strength -= strength;
        this.dexterity -= dexterity;
        this.intelligence -= intelligence;
    }

    //getter and setters
    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }
}
