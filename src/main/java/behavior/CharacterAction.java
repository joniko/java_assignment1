package main.java.behavior;

import main.java.attributes.Attributes;
import main.java.exception.InvalidArmorException;
import main.java.exception.InvalidWeaponException;
import main.java.item.Armor;
import main.java.item.Weapon;

//Interface for characters. Includes all required methods.
public interface CharacterAction {
    abstract void levelUp();
    boolean equip(Weapon weapon) throws InvalidWeaponException;
    boolean equip(Armor armor) throws InvalidArmorException;
    double attack();
    String displayStats();
}
