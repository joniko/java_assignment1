package main.java.character;

import main.java.attributes.Attributes;
import main.java.behavior.CharacterAction;
import main.java.exception.InvalidArmorException;
import main.java.exception.InvalidWeaponException;
import main.java.item.Armor;
import main.java.item.Weapon;
import main.java.item.Item;

import java.util.HashMap;

//Parent class which includes mostly helper methods to child classes.
//Class includes also dummy constructor and interface methods in dummy form.
public abstract class Character implements CharacterAction{

    private String name;
    private int level;
    private Attributes primaryAttribute;
    private Attributes totalAttribute;
    private HashMap<Armor.Slot, Armor> equipments;
    private HashMap<Weapon.Slot, Weapon> equippedWeapon;

    //constructor
    public Character(String name) {
        this.name = name;
        this.level = 1;
        this.primaryAttribute = new Attributes(1, 1, 1);
        this.totalAttribute = new Attributes(1,1,1);
        this.equipments = new HashMap<Armor.Slot, Armor>();
        this.equippedWeapon = new HashMap<Weapon.Slot, Weapon>();
    }

    //Dummy method for character level up. It's override in all child classes.
    public void levelUp(){
    }

    //Dummy method for character equipping weapon. It's override in all child classes.
    public boolean equip(Weapon weapon) throws InvalidWeaponException{
        return false;
    }
    //Dummy method for character equipping armor. It's override in all child classes.
    public boolean equip(Armor armor) throws InvalidArmorException {
        return false;
    }

    //Dummy method for attack method. Gives DPS as output.
    public double attack(){
        return 0;
    }

    //Dummy method for displaying stats. It's overwritten in child classes.
    public String displayStats(){
        return "";
    }

    //Helper method for equipping armor. It checks if armor slot is empty and places item to the slot.
    //Or replaces armor in certain slot. Method also changes the character attributes in totalAttribute property.
    public void helperArmorEquip(Armor armor, Attributes attr){
        if(equipments.get(armor.getSlot()) == null){
            System.out.println("Yea! Armor equipped to " + armor.getSlot());
            attr.addAttributes(armor.getArmorAttributes().getStrength(),
                    armor.getArmorAttributes().getDexterity(),
                    armor.getArmorAttributes().getIntelligence());
            equipments.put(armor.getSlot(), armor);
        }
        else{
            System.out.println("You changed the " + armor.getSlot() +  " Armor");
            Attributes toRemove = equipments.get(armor.getSlot()).getArmorAttributes();
            attr.removeAttributes(toRemove.getStrength(),
                    toRemove.getDexterity(),
                    toRemove.getIntelligence());
            attr.addAttributes(armor.getArmorAttributes().getStrength(),
                    armor.getArmorAttributes().getDexterity(),
                    armor.getArmorAttributes().getIntelligence());
            equipments.replace(armor.getSlot(), armor);
        }
    }

    //helper method for displayStats(). Used in child classes.
    public String createStats(Attributes attr){
        String stats = "Name: " + name +
                "\nLevel: " + level +
                "\nStrenght: " + attr.getStrength() +
                "\nDexterity: " + attr.getDexterity() +
                "\nInteligence: " + attr.getIntelligence() +
                "\nDPS: " + attack();
        return stats;
    }


}
