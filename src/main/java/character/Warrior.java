package main.java.character;

import main.java.attributes.Attributes;
import main.java.exception.InvalidArmorException;
import main.java.exception.InvalidWeaponException;
import main.java.item.Armor;
import main.java.item.Item;
import main.java.item.Weapon;

import java.util.HashMap;

//Class for making warrior objects.
public class Warrior extends Character{

    private String name;
    private int level;
    private Attributes primaryAttribute;
    private Attributes totalAttribute;
    private HashMap<Armor.Slot, Armor> equipments;
    private HashMap<Weapon.Slot, Weapon> equippedWeapon;

    //constructor
    public Warrior(String name) {
        super(name);
        this.level = 1;
        this.primaryAttribute = new Attributes(5,2,1);
        this.totalAttribute = new Attributes(5,2,1);
        this.equipments = new HashMap<Armor.Slot, Armor>();
        this.equippedWeapon = new HashMap<Weapon.Slot, Weapon>();
    }

    //Method levels up warrior. Also adds needed attributes in primary and total attributes.
    @Override
    public void levelUp() {
        System.out.println("Leveled up");
        level += 1;
        primaryAttribute.addAttributes(3, 2, 1);
        totalAttribute.addAttributes(3,2,1);
    }

    //Method to equip armor to the warrior. Check armors legality and uses Character classes helper method for equipping.
    //In illegal case of equipping -> throws InvalidArmorException.
    public boolean equip(Armor armor) throws InvalidArmorException {
        if(armor.getType() == Armor.ArmorType.CLOTH ||
                armor.getType() == Armor.ArmorType.LEATHER ||
                armor.getLevel() > level) {
            throw new InvalidArmorException();
        }
        helperArmorEquip(armor, totalAttribute);
        return true;
    }

    //Method to equip/replace weapon to the warrior. Checks weapons legality for equipping.
    //In illegal case of equipping -> throws InvalidWeaponException.
    public boolean equip(Weapon weapon) throws InvalidWeaponException {
        if(weapon.getType() == Weapon.WeaponType.BOW ||
                weapon.getType() == Weapon.WeaponType.DAGGER ||
                weapon.getType() == Weapon.WeaponType.STAFF ||
                weapon.getType() == Weapon.WeaponType.WAND ||
                weapon.getLevel() > level) {
            throw new InvalidWeaponException();
        }
        equippedWeapon.put(weapon.getSlot(), weapon);
        return true;
    }

    //Method performs warriors attack. Output is the total DPS in double.
    public double attack(){
        if (equippedWeapon.get(Weapon.Slot.WEAPON) == null){
            return 1*(1 + Double.valueOf(totalAttribute.getStrength()/100.00));
        }
        return equippedWeapon.get(Weapon.Slot.WEAPON).getWeaponDPS() * (1 + Double.valueOf(totalAttribute.getStrength()/100.00));
    }

    //Returns warriors total stats in String format. Method uses Character classes helper method createStats().
    @Override
    public String displayStats() {
        return createStats(totalAttribute);
    }

    //Getter & Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Attributes getPrimaryAttribute() {
        return primaryAttribute;
    }

    public void setPrimaryAttribute(Attributes primaryAttribute) {
        this.primaryAttribute = primaryAttribute;
    }

    public Attributes getTotalAttribute() {
        return totalAttribute;
    }

    public void setTotalAttribute(Attributes totalAttribute) {
        this.totalAttribute = totalAttribute;
    }

    public HashMap<Armor.Slot, Armor> getEquipments() {
        return equipments;
    }

    public void setEquipments(HashMap<Armor.Slot, Armor> equipments) {
        this.equipments = equipments;
    }

    public HashMap<Weapon.Slot, Weapon> getEquippedWeapon() {
        return equippedWeapon;
    }

    public void setEquippedWeapon(HashMap<Weapon.Slot, Weapon> equippedWeapon) {
        this.equippedWeapon = equippedWeapon;
    }
}
