package main.java.item;

//Abstract class for Armor and Weapons. Includes the armor and weapon slots.
public class Item {

    public enum Slot {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }

    private String name;
    private int level;
    private Slot slot;

    public Item(String name, int level, Slot slot) {
        this.name = name;
        this.level = level;
        this.slot = slot;
    }

    //getter and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

}
